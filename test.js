const { expect } = require('chai');

const BowlingGame = require('./index.js');

describe('BowlingGame', () => {
    let game;
    beforeEach(() => {
        game = new BowlingGame();
    });

    function rollMany(rolls, pins) {
        for (i = 0; i < rolls; i++) {
            game.roll(pins);
        }
    }

    function rollSpare() {
        game.roll(5);
        game.roll(5);
    }

    function rollStrike() {
        game.roll(10);
    }

    describe('all gutter-ball', () => {
        beforeEach(() => {
            rollMany(20, 0);
        });
        it('should have a score of 0', () => {
            expect(game.score).to.equal(0);
        });
    });

    describe('all 1s', () => {
        beforeEach(() => {
            rollMany(20, 1);
        });
        it('should have a score of 20', () => {
            expect(game.score).to.equal(20);
        });
    });

    describe('roll a spare', () => {
        beforeEach(() => {
            rollSpare();
            game.roll(3);
            rollMany(17, 0);
        });

        it('should add points from the next frame to the spare', () => {
            expect(game.score).to.equal(16);
        });
    });

    describe('roll a strike', () => {
        beforeEach(() => {
            rollStrike();
            game.roll(3);
            game.roll(4);
            rollMany(16, 0);
        });

        it('should add points from the next two frames to the strike', () => {
            expect(game.score).to.equal(24);
        });
    });

    describe('the perfect game', () => {
        beforeEach(() => {
            rollMany(12, 10);
        });

        it('should get the perfect score of 300', () => {
            expect(game.score).to.equal(300);
        });
    });
});
