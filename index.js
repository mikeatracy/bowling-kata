class BowlingGame {
    constructor() {
        this.rolls = [];
        this.currentRole = 0;
    }

    roll(pins) {
        this.rolls[this.currentRole] = pins;
        this.currentRole += 1;
    }

    get score() {
        let score = 0;
        let frameIndex = 0;
        // get pins per frame
        for (let frame = 0; frame < 10; frame++) {
            if (this._isStrike(frameIndex)) { // is a strike
                score += 10 + this._strikeBonus(frameIndex);
                frameIndex += 1;
            } else if (this._isSpare(frameIndex)) {
                score += 10 + this._spareBonus(frameIndex);
                frameIndex += 2;
            } else {
                score += this._sumFrame(frameIndex);
                frameIndex += 2;
            }
        }
        return score;
    }

    _sumFrame(frameIndex) {
        let roll1 = this.rolls[frameIndex];
        let roll2 = this.rolls[frameIndex + 1];
        return roll1 + roll2;
    }

    _spareBonus(frameIndex) {
        return this.rolls[frameIndex + 2];
    }

    _strikeBonus(frameIndex) {
        return this._sumFrame(frameIndex + 1);
    }

    _isSpare(frameIndex) {
        return this._sumFrame(frameIndex) === 10;
    }

    _isStrike(frameIndex) {
        return this.rolls[frameIndex] === 10;
    }
}

module.exports = BowlingGame
